/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maps;

import java.io.UnsupportedEncodingException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.net.URLEncoder;

/**
 *
 * @author Andi
 */
@WebService(serviceName = "BookShopWebService")
public class BookShopWebService {
    /**
     * Web service operation
     */
    @WebMethod(operationName = "location")
    public String location(@WebParam(name = "address") String address) {
        //address.replace(' ', '+')
        try {
            String url = "http://maps.google.com/?q=" + URLEncoder.encode(address, "UTF-8");
            return url;
        }
        catch (UnsupportedEncodingException e) {
            return e.getMessage();
        }
    }
}
