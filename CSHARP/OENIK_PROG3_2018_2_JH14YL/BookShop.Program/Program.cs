﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;
    using BookShop.Logic;
    using BookShop.Logic.Interfaces;
    using BookShop.Program.ConsoleMenu;
    using BookShop.Program.MapsServiceReference;
    using BookShop.Repository;
    using BookShop.Repository.Repositories;

    /// <summary>
    /// The entry point.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            using (BookShopDBEntities entities = new BookShopDBEntities())
            {
                using (BookShopWebServiceClient proxy = new BookShopWebServiceClient())
                {
                    Controller controller = new Controller(new BasicInfosRepository(entities), new ShopRepository(entities), new CategoriesRepository(entities), proxy);
                    ConsoleManager console = new ConsoleManager();
                    console.AddHandler("List all books", controller.BookList);
                    console.AddHandler("List all shops", controller.ShopList);
                    console.AddHandler("Add a new book", controller.AddBook);
                    console.AddHandler("Add a new shop", controller.AddShop);
                    console.AddHandler("Delete a book", controller.DeleteBook);
                    console.AddHandler("Delete a shop", controller.DeleteShop);
                    console.AddHandler("Update a book", controller.ModifyBook);
                    console.AddHandler("Update a shop", controller.ModifyShop);
                    console.AddHandler("List (at most) 3 least read books", controller.LeastReadBooks);
                    console.AddHandler("Filter books", controller.FilterBooks);
                    console.AddHandler("Show a book on Google Maps", controller.ShopsOnGoogleMaps);

                    console.Run();
                }
            }
        }
    }
}
