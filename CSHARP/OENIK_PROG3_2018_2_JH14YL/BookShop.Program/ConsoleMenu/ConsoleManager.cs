﻿// <copyright file="ConsoleManager.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Program.ConsoleMenu
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Console manager menu.
    /// </summary>
    public class ConsoleManager
    {
        /// <summary>
        /// List of menus.
        /// </summary>
        private readonly List<MenuOption> menus = new List<MenuOption>();

        /// <summary>
        /// The console menu is running until variable is true.
        /// </summary>
        private bool running = true;

        /// <summary>
        /// Making pairs - menu item description and calling the proper method.
        /// </summary>
        /// <param name="message">Menu item description.</param>
        /// <param name="handler">Method to call when menu item is selected.</param>
        public void AddHandler(string message, Action handler)
        {
            this.menus.Add(new MenuOption(message, handler));
        }

        /// <summary>
        /// Have to call this method to start menu manager.
        /// </summary>
        public void Run()
        {
            this.AddHandler("Exit", this.Stop);
            while (this.running)
            {
                this.Display();
                Console.WriteLine();
                this.WaitForInput();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Stopping console menu.
        /// </summary>
        private void Stop()
        {
            this.running = false;
        }

        /// <summary>
        /// Displays all the menu items.
        /// </summary>
        private void Display()
        {
            Console.WriteLine("Pick from the menu items:");
            for (int i = 0; i < this.menus.Count; i++)
            {
                Console.WriteLine(string.Format("{0}. {1}", i + 1, this.menus[i].Message));
            }
        }

        /// <summary>
        /// Waiting for user input to choose from menus.
        /// </summary>
        private void WaitForInput()
        {
            int userChoice = 0;
            while (userChoice <= 0 || userChoice > this.menus.Count)
            {
                Console.Write("Choose menu: ");
                if (!int.TryParse(Console.ReadLine(), out userChoice))
                {
                    userChoice = 0;
                }
            }

            this.menus[userChoice - 1].Action();
        }

        /// <summary>
        /// Represents a menu item with the description and a callback.
        /// </summary>
        private class MenuOption
        {
            /// <summary>
            /// The method to call.
            /// </summary>
            private Action action;

            /// <summary>
            /// Description of the menu item.
            /// </summary>
            private string message;

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuOption"/> class.
            /// Setting MenuOption.
            /// </summary>
            /// <param name="message">Description of menu item.</param>
            /// <param name="action">Method to call.</param>
            public MenuOption(string message, Action action)
            {
                this.Message = message;
                this.Action = action;
            }

            /// <summary>
            /// Gets or sets the method to call.
            /// </summary>
            public Action Action { get => this.action; set => this.action = value; }

            /// <summary>
            /// Gets or sets the menu item description.
            /// </summary>
            public string Message { get => this.message; set => this.message = value; }
        }
    }
}
