﻿// <copyright file="Controller.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Program
{
    using System;
    using BookShop.Data;
    using BookShop.Logic;
    using BookShop.Logic.Interfaces;
    using BookShop.Program.MapsServiceReference;
    using BookShop.Repository;

    /// <summary>
    /// Menu controller class. This class calls the logic.
    /// </summary>
    public class Controller
    {
        private readonly IRepository<BasicInfos> bookRepository;
        private readonly IRepository<Shops> shopRepository;
        private readonly IRepository<Categories> categoriesRepository;

        private readonly IBookListLogic bookListLogic;
        private readonly IShopListLogic shopListLogic;
        private readonly IBookAndCategorySelectionLogic bookAndCategorySelectionLogic;
        private readonly BookShopWebServiceClient proxy;

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller"/> class.
        /// Sets the appropriate attributes.
        /// </summary>
        /// <param name="bookRepository">Book repository.</param>
        /// <param name="shopRepository">Shop repository.</param>
        /// <param name="categoriesRepository">Category repository.</param>
        /// <param name="proxy">Service client to Java endpoint.</param>
        public Controller(IRepository<BasicInfos> bookRepository, IRepository<Shops> shopRepository, IRepository<Categories> categoriesRepository, BookShopWebServiceClient proxy)
        {
            this.bookRepository = bookRepository;
            this.shopRepository = shopRepository;
            this.categoriesRepository = categoriesRepository;
            this.proxy = proxy;

            this.bookListLogic = new BookListLogic(this.bookRepository);
            this.shopListLogic = new ShopListLogic(this.shopRepository);
            this.bookAndCategorySelectionLogic = new BookAndCategorySelectionLogic(this.categoriesRepository);
        }

        /// <summary>
        /// Lists and writes the books to console.
        /// </summary>
        public void BookList()
        {
            Console.WriteLine("Books:");
            foreach (var book in this.bookListLogic.GetFilteredBookList(null))
            {
                Console.WriteLine(book.ToString());
            }
        }

        /// <summary>
        /// Lists and writes the shops to console.
        /// </summary>
        public void ShopList()
        {
            Console.WriteLine("Shops:");
            foreach (var shop in this.shopListLogic.Shops)
            {
                Console.WriteLine(string.Format("Id: {0}, City: {1}, Address: {2}", shop.Id, shop.City, shop.Address));
            }
        }

        /// <summary>
        /// Add new book to repository.
        /// </summary>
        public void AddBook()
        {
            BasicInfos basicInfos = new BasicInfos();

            Console.Write("Author: ");
            basicInfos.Title = Console.ReadLine();
            Console.Write("Title: ");
            basicInfos.Author = Console.ReadLine();
            Console.Write("Description: ");
            basicInfos.Description = Console.ReadLine();
            Console.Write("Price: ");
            basicInfos.Price = decimal.Parse(Console.ReadLine());
            Console.Write("Picture URL: ");
            basicInfos.PictureUrl = Console.ReadLine();
            Console.WriteLine("Categories: ");

            foreach (var category in this.bookAndCategorySelectionLogic.AllCategories)
            {
                Console.WriteLine(category.Name);
            }

            Console.Write("Provide category: ");
            basicInfos.Categories = this.bookAndCategorySelectionLogic.GetCategoryByName(Console.ReadLine());
            while (basicInfos.Categories == null)
            {
                Console.Write("Invalid category, try again: ");
                basicInfos.Categories = this.bookAndCategorySelectionLogic.GetCategoryByName(Console.ReadLine());
            }

            basicInfos.Extras = new Extras();
            Console.Write("Language: ");
            basicInfos.Extras.Language = Console.ReadLine();
            Console.Write("Number of pieces: ");
            basicInfos.Extras.Piece = int.Parse(Console.ReadLine());

            this.bookListLogic.Save(basicInfos);
        }

        /// <summary>
        /// Add new shop to repository.
        /// </summary>
        public void AddShop()
        {
            Shops shop = new Shops();

            Console.Write("Shop city: ");
            shop.City = Console.ReadLine();
            Console.Write("Shop address: ");
            shop.Address = Console.ReadLine();
            Console.Write("Shop name: ");
            shop.ShopName = Console.ReadLine();
            Console.Write("Shop owner: ");
            shop.ShopOwner = Console.ReadLine();

            this.shopListLogic.Save(shop);
        }

        /// <summary>
        /// Delete book from repository.
        /// </summary>
        public void DeleteBook()
        {
            Console.Write("ID of the book to delete: ");
            int id = int.Parse(Console.ReadLine());
            BasicInfos book = this.bookListLogic.GetBookById(id);
            if (book == null)
            {
                Console.WriteLine("ID not found!");
            }
            else
            {
                this.bookListLogic.Delete(book);
                Console.WriteLine("Book deleted");
            }
        }

        /// <summary>
        /// Delete shop from repository.
        /// </summary>
        public void DeleteShop()
        {
            Console.Write("ID of the shop to delete: ");
            int id = int.Parse(Console.ReadLine());
            Shops shop = this.shopListLogic.GetShopById(id);
            if (shop == null)
            {
                Console.WriteLine("ID not found!");
            }
            else
            {
                this.shopListLogic.Delete(shop);
                Console.WriteLine("Book deleted");
            }
        }

        /// <summary>
        /// Modify book in repository.
        /// </summary>
        public void ModifyBook()
        {
            Console.Write("ID of the book to modify: ");
            int id = int.Parse(Console.ReadLine());
            BasicInfos basicInfos = this.bookListLogic.GetBookById(id);

            if (basicInfos == null)
            {
                Console.WriteLine("ID not found!");
            }
            else
            {
                basicInfos.Author = this.ModifyStringField("Author", basicInfos.Author);
                basicInfos.Title = this.ModifyStringField("Title", basicInfos.Title);
                basicInfos.Description = this.ModifyStringField("Description", basicInfos.Description);
                basicInfos.Price = int.Parse(this.ModifyStringField("Price", basicInfos.Price.ToString()));
                basicInfos.PictureUrl = this.ModifyStringField("PictureUrl", basicInfos.PictureUrl);

                Console.WriteLine(string.Format("Category: {0}", basicInfos.Categories.Name));
                string input = string.Empty;
                while (!input.Equals("y") && !input.Equals("n"))
                {
                    Console.Write("Modify (y/n): ");
                    input = Console.ReadLine();
                }

                if (input.Equals("y"))
                {
                    Console.WriteLine("Categories: ");

                    foreach (var category in this.bookAndCategorySelectionLogic.AllCategories)
                    {
                        Console.WriteLine(category.Name);
                    }

                    Console.Write("New category: ");
                    basicInfos.Categories = this.bookAndCategorySelectionLogic.GetCategoryByName(Console.ReadLine());
                    while (basicInfos.Categories == null)
                    {
                        Console.Write("Invalid category, try again: ");
                        basicInfos.Categories = this.bookAndCategorySelectionLogic.GetCategoryByName(Console.ReadLine());
                    }

                    this.bookListLogic.Save(basicInfos);
                }

                basicInfos.Extras.Language = this.ModifyStringField("Language", basicInfos.Extras.Language);
                basicInfos.Extras.Piece = int.Parse(this.ModifyStringField("Number of pieces", basicInfos.Extras.Piece.ToString()));
            }
        }

        /// <summary>
        /// Modify shop in repository.
        /// </summary>
        public void ModifyShop()
        {
            Console.Write("ID of the book to modify: ");
            int id = int.Parse(Console.ReadLine());
            Shops shop = this.shopListLogic.GetShopById(id);

            if (shop == null)
            {
                Console.WriteLine("ID not found!");
            }
            else
            {
                shop.City = this.ModifyStringField("City", shop.City);
                shop.Address = this.ModifyStringField("Address", shop.Address);
                shop.ShopName = this.ModifyStringField("ShopName", shop.ShopName);
                shop.ShopOwner = this.ModifyStringField("ShopOwner", shop.ShopOwner);
            }
        }

        /// <summary>
        /// Returns 3 least read books.
        /// </summary>
        public void LeastReadBooks()
        {
            var books = this.bookListLogic.GetLeastRead(3);
            Console.WriteLine("3 least read books");
            foreach (var book in books)
            {
                Console.WriteLine(string.Format("Id: {0}, Author: {1}, Title: {2}", book.Id, book.Author, book.Title));
            }
        }

        /// <summary>
        /// Filtering option for books.
        /// </summary>
        public void FilterBooks()
        {
            Console.WriteLine("Type an empty line to ignore a filter");

            Console.Write("Language: ");
            string language = Console.ReadLine();

            Console.Write("Min price: ");
            string minPrice = Console.ReadLine();

            Console.Write("Max price: ");
            string maxPrice = Console.ReadLine();

            Console.WriteLine("Categories: ");
            foreach (var c in this.bookAndCategorySelectionLogic.AllCategories)
            {
                Console.WriteLine(c.Name);
            }

            Console.Write("Category: ");
            string category = Console.ReadLine();
            Categories categoryObj = this.bookAndCategorySelectionLogic.GetCategoryByName(category);
            while (categoryObj == null && !string.IsNullOrEmpty(category))
            {
                Console.Write("Invalid category, try again: ");
                category = Console.ReadLine();
                categoryObj = this.bookAndCategorySelectionLogic.GetCategoryByName(category);
            }

            FilterSet filters = new FilterSet(
                language,
                string.IsNullOrEmpty(minPrice) ? (uint?)null : uint.Parse(minPrice),
                string.IsNullOrEmpty(maxPrice) ? (uint?)null : uint.Parse(maxPrice),
                category);

            var books = this.bookListLogic.GetFilteredBookList(filters);
            Console.WriteLine("Filtered books");
            foreach (var book in books)
            {
                Console.WriteLine(book.ToString());
            }
        }

        /// <summary>
        /// Showing location of the given shop if possible.
        /// </summary>
        public void ShopsOnGoogleMaps()
        {
            Console.Write("ID of the shop to show: ");
            int id = int.Parse(Console.ReadLine());
            Shops shop = this.shopListLogic.GetShopById(id);
            if (shop == null)
            {
                Console.WriteLine("ID not found!");
            }
            else
            {
                string location = this.proxy.location(shop.Address);
                Console.WriteLine(location);
            }
        }

        /// <summary>
        /// Helper function to modify fields with error checking.
        /// </summary>
        /// <param name="name">Name of the field.</param>
        /// <param name="field">Value of the field.</param>
        /// <returns>Field.</returns>
        private string ModifyStringField(string name, string field)
        {
            Console.WriteLine(string.Format("{0}: {1}", name, field));
            string input = string.Empty;
            while (!input.Equals("y") && !input.Equals("n"))
            {
                Console.Write("Modify (y/n): ");
                input = Console.ReadLine();
            }

            if (input.Equals("y"))
            {
                Console.Write(string.Format("New {0}: ", name));
                return Console.ReadLine();
            }

            return field;
        }
    }
}
