﻿// <copyright file="CategoriesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Repository of categories.
    /// </summary>
    public class CategoriesRepository : IRepository<Categories>
    {
        /// <summary>
        /// Entities of DB.
        /// </summary>
        private readonly BookShopDBEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoriesRepository"/> class.
        /// Repository of categories.
        /// </summary>
        /// <param name="entities">Entities of DB.</param>
        public CategoriesRepository(BookShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Gets all the categories.
        /// </summary>
        public IQueryable<Categories> All => this.entities.Categories;

        /// <summary>
        /// Deleting category.
        /// </summary>
        /// <param name="t">Category.</param>
        public void Delete(Categories t)
        {
            this.entities.Categories.Remove(t);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Saving category.
        /// </summary>
        /// <param name="t">Category.</param>
        public void Save(Categories t)
        {
            bool newCategory = !this.entities.Categories.Select(m => m.Id).Contains(t.Id);
            if (newCategory)
            {
                t.Id = (this.entities.Categories.Max(m => (int?)m.Id) ?? 0) + 1;
            }

                if (newCategory)
                {
                    this.entities.Categories.Add(t);
                }

                this.entities.SaveChanges();
        }
    }
}
