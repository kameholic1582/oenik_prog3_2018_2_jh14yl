﻿// <copyright file="BasicInfosRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Repository of books.
    /// </summary>
    public class BasicInfosRepository : IRepository<BasicInfos>
    {
        /// <summary>
        /// Entities.
        /// </summary>
        private readonly BookShopDBEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicInfosRepository"/> class.
        /// Setting repository.
        /// </summary>
        /// <param name="entities">Entities.</param>
        public BasicInfosRepository(BookShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Gets the basicinfos to its entity.
        /// </summary>
        public IQueryable<BasicInfos> All => this.entities.BasicInfos;

        /// <summary>
        /// Deleting the entity from DB and saving changes.
        /// </summary>
        /// <param name="t">Book.</param>
        public void Delete(BasicInfos t)
        {
            this.entities.BasicInfos.Remove(t);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Saving new book.
        /// </summary>
        /// <param name="t">Book.</param>
        public void Save(BasicInfos t)
        {
            bool newBook = !this.entities.BasicInfos.Select(m => m.Id).Contains(t.Id);
            if (newBook)
            {
                t.Id = (this.entities.BasicInfos.Max(m => (int?)m.Id) ?? 0) + 1;
            }

            try
            {
                if (newBook)
                {
                    this.entities.BasicInfos.Add(t);
                }

                this.entities.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (newBook)
                {
                    this.entities.BasicInfos.Remove(t);
                }

                this.entities.Entry(t).Reload();

                throw;
            }
        }
    }
}
