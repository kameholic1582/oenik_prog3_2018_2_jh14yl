﻿// <copyright file="ShopRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Repository of shops.
    /// </summary>
    public class ShopRepository : IRepository<Shops>
    {
        /// <summary>
        /// DB entities.
        /// </summary>
        private readonly BookShopDBEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopRepository"/> class.
        /// Setting shoprepository.
        /// </summary>
        /// <param name="entities">DDB entities.</param>
        public ShopRepository(BookShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Gets all the shops.
        /// </summary>
        public IQueryable<Shops> All => this.entities.Shops;

        /// <summary>
        /// Delete shop.
        /// </summary>
        /// <param name="t">Shop.</param>
        public void Delete(Shops t)
        {
            this.entities.Shops.Remove(t);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Save shop.
        /// </summary>
        /// <param name="t">Shop.</param>
        public void Save(Shops t)
        {
            bool newShop = !this.entities.Shops.Select(m => m.Id).Contains(t.Id);
            if (newShop)
            {
                t.Id = (this.entities.Shops.Max(m => (int?)m.Id) ?? 0) + 1;
            }

            try
            {
                if (newShop)
                {
                    this.entities.Shops.Add(t);
                }

                this.entities.SaveChanges();
            }
            catch
            {
                if (newShop)
                {
                    this.entities.Shops.Remove(t);
                }

                this.entities.Entry(t).Reload();

                throw;
            }
        }
    }
}
