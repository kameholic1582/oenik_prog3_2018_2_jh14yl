﻿// <copyright file="ExtrasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Repository.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Repository of extras.
    /// </summary>
    public class ExtrasRepository : IRepository<Extras>
    {
        /// <summary>
        /// DB entities.
        /// </summary>
        private readonly BookShopDBEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrasRepository"/> class.
        /// Setting extra repository.
        /// </summary>
        /// <param name="entities">DB entities.</param>
        public ExtrasRepository(BookShopDBEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Gets all the extras.
        /// </summary>
        public IQueryable<Extras> All => this.entities.Extras;

        /// <summary>
        /// Delete extras.
        /// </summary>
        /// <param name="t">Extra.</param>
        public void Delete(Extras t)
        {
            this.entities.Extras.Remove(t);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Save extra.
        /// </summary>
        /// <param name="t">Extra.</param>
        public void Save(Extras t)
        {
            bool newExtra = !this.entities.Extras.Select(m => m.Id).Contains(t.Id);
            if (newExtra)
            {
                t.Id = (this.entities.Extras.Max(m => (int?)m.Id) ?? 0) + 1;
            }

            try
            {
                if (newExtra)
                {
                    this.entities.Extras.Add(t);
                }

                this.entities.SaveChanges();
            }
            catch
            {
                if (newExtra)
                {
                    this.entities.Extras.Remove(t);
                }

                this.entities.Entry(t).Reload();

                throw;
            }
        }
    }
}
