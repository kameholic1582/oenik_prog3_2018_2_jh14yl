﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository class.
    /// </summary>
    /// <typeparam name="T">The type of the repository.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets the repositories.
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Save repository.
        /// </summary>
        /// <param name="t">The type of the repository.</param>
        void Save(T t);

        /// <summary>
        /// Delete repository.
        /// </summary>
        /// <param name="t">The type of the repository.</param>
        void Delete(T t);
    }
}
