﻿// <copyright file="TestBookListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using BookShop.Data;
    using BookShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Testing book list logic.
    /// </summary>
    [TestFixture]
    public class TestBookListLogic
    {
        private Mock<IRepository<BasicInfos>> mockBookRepo;
        private List<BasicInfos> books = new List<BasicInfos>();
        private BookListLogic bookListLogic;

        /// <summary>
        /// Setting up test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.books = new List<BasicInfos>
            {
                new BasicInfos()
                {
                    Id = 1,
                    Title = "Édes emlékek anyukámtól",
                    Author = "Bernáth József",
                    Description = "Finomabbnál finomabb sütemények.",
                    Price = 5490,
                    PictureUrl = "https://s05.static.libri.hu/cover/48/9/4914549_5.jpg",
                    Extras = new Extras()
                    {
                        Language = "English",
                        Piece = 3
                    },
                    Categories = new Categories()
                    {
                        Name = "Gastronomie"
                    }
                },
                new BasicInfos()
                {
                    Id = 2,
                    Title = "Tanítsd meg tanulni! - A Lépéselőny tanulási stratégia szülőknek",
                    Author = "Lantos Mihály",
                    Description = "A legtöbb, amit tehetünk a gyerekünkért, az az, hogy nem engedjük elveszíteni a tanulási képességeit! Ha hagyjuk, hogy vidáman, játékosan fedezze fel ő maga a világot, és ha kell, mutatunk neki olyan tanulási eszközöket és módszereket, amelyek az elménk működéséhez igazodnak, nem pedig az elméjét akarjuk rákényszeríteni értelmetlen szabályok alkalmazására. Ha ezt megtettük, elvégeztük a vállalt feladatunkat. A hatékony módszerek használata, vagyis a tanulás már a gyerek dolga!",
                    Price = 2300,
                    PictureUrl = "https://s04.static.libri.hu/cover/91/6/4915540_5.jpg",
                    Extras = new Extras()
                    {
                        Language = "Magyar",
                        Piece = 1
                    },
                    Categories = new Categories()
                    {
                        Name = "Family"
                    }
                }
            };
            this.mockBookRepo = new Mock<IRepository<BasicInfos>>();

            this.mockBookRepo.Setup(r => r.All).Returns(this.books.AsQueryable);
            this.mockBookRepo.Setup(r => r.Save(It.IsAny<BasicInfos>())).Callback(
                (BasicInfos book) =>
                {
                    var find = this.books.Find(b => b.Id == book.Id);
                    if (find != null)
                    {
                        find.Id = book.Id;
                        find.Title = book.Title;
                        find.Author = book.Author;
                    }
                    else
                    {
                        this.books.Add(book);
                    }
                });
            this.mockBookRepo.Setup(r => r.Delete(It.IsAny<BasicInfos>())).Callback(
                (BasicInfos book) =>
                {
                    this.books.RemoveAll(b => b.Id == book.Id);
                });

            this.bookListLogic = new BookListLogic(this.mockBookRepo.Object);
        }

        /// <summary>
        /// Testing if we can get all books.
        /// </summary>
        [Test]
        public void BookListLogic_GetAll()
        {
            var books = this.bookListLogic.GetFilteredBookList(null);

            Assert.AreEqual(books.Count(), this.books.Count);
            Assert.AreEqual(books.ElementAt(0).Title, this.books.ElementAt(0).Title);
            Assert.AreEqual(books.ElementAt(0).Author, this.books.ElementAt(0).Author);
            Assert.AreEqual(books.ElementAt(1).Title, this.books.ElementAt(1).Title);
            Assert.AreEqual(books.ElementAt(1).Author, this.books.ElementAt(1).Author);
        }

        /// <summary>
        /// Testing empty filters.
        /// </summary>
        [Test]
        public void BookListLogic_GetWithEmptyFilters()
        {
            FilterSet filters = new FilterSet(null, null, null, null);
            var books = this.bookListLogic.GetFilteredBookList(filters);

            Assert.AreEqual(books.Count(), this.books.Count);
            Assert.AreEqual(books.ElementAt(0).Title, this.books.ElementAt(0).Title);
            Assert.AreEqual(books.ElementAt(0).Author, this.books.ElementAt(0).Author);
            Assert.AreEqual(books.ElementAt(1).Title, this.books.ElementAt(1).Title);
            Assert.AreEqual(books.ElementAt(1).Author, this.books.ElementAt(1).Author);
        }

        /// <summary>
        /// Testing price filters.
        /// </summary>
        [Test]
        public void BookListLogic_GetWithPriceFilters()
        {
            FilterSet filters = new FilterSet(null, null, null, null)
            {
                LanguageFilter = null,
                MinPriceFilter = 2000
            };
            Assert.AreEqual(this.bookListLogic.GetFilteredBookList(filters).Count(), 2);
            filters.MinPriceFilter = 6000;
            Assert.AreEqual(this.bookListLogic.GetFilteredBookList(filters).Count(), 0);

            filters.MinPriceFilter = 0;
            filters.MaxPriceFilter = 2000;
            Assert.AreEqual(this.bookListLogic.GetFilteredBookList(filters).Count(), 0);

            filters.MinPriceFilter = 2000;
            filters.MaxPriceFilter = 6000;
            Assert.AreEqual(this.bookListLogic.GetFilteredBookList(filters).Count(), 2);
        }

        /// <summary>
        /// Testing language filters.
        /// </summary>
        [Test]
        public void BookListLogic_GetWithLanguageFilters()
        {
            FilterSet filters = new FilterSet("English", null, null, null);
            var books = this.bookListLogic.GetFilteredBookList(filters);

            Assert.AreEqual(books.Count(), 1);
            Assert.AreEqual(books.ElementAt(0).Title, "Édes emlékek anyukámtól");
        }

        /// <summary>
        /// Testing if we can get books with category filters.
        /// </summary>
        [Test]
        public void BookListLogic_GetWithCategoryFilters()
        {
            FilterSet filters = new FilterSet(null, null, null, "Family");
            var books = this.bookListLogic.GetFilteredBookList(filters);

            Assert.AreEqual(books.Count(), 1);
            Assert.AreEqual(books.ElementAt(0).Categories.Name, "Family");
        }

        /// <summary>
        /// Testing if we can delete.
        /// </summary>
        [Test]
        public void BookListLogic_Delete()
        {
            this.bookListLogic.Delete(new BasicInfos() { Id = 1 });
            var books = this.bookListLogic.GetFilteredBookList(null);

            Assert.AreEqual(books.Count, 1);
            Assert.AreEqual(books.ElementAt(0).Id, 2);
        }

        /// <summary>
        /// Testing if we can add books.
        /// </summary>
        [Test]
        public void BookListLogic_Add()
        {
            this.bookListLogic.Save(new BasicInfos()
            {
                Id = 3,
                Title = "title",
                Author = "author"
            });
            var shops = this.bookListLogic.GetFilteredBookList(null);
            Assert.AreEqual(shops.Count, 3);

            var newShop = shops.ElementAt(2);
            Assert.AreEqual(newShop.Id, 3);
            Assert.AreEqual(newShop.Title, "title");
            Assert.AreEqual(newShop.Author, "author");
        }

        /// <summary>
        /// Testing if we can modify books.
        /// </summary>
        [Test]
        public void BookListLogic_Modify()
        {
            this.bookListLogic.Save(new BasicInfos()
            {
                Id = 1,
                Title = "modTitle",
                Author = "modAuthor"
            });
            var books = this.bookListLogic.GetFilteredBookList(null);
            Assert.AreEqual(books.Count, 2);

            var modBook = books.ElementAt(0);
            Assert.AreEqual(modBook.Id, 1);
            Assert.AreEqual(modBook.Title, "modTitle");
            Assert.AreEqual(modBook.Author, "modAuthor");
        }

        /// <summary>
        /// Testing if we can get book by id.
        /// </summary>
        [Test]
        public void BookListLogic_GetBookById()
        {
            var book = this.bookListLogic.GetBookById(1);

            Assert.AreEqual(book.Title, "Édes emlékek anyukámtól");
        }

        /// <summary>
        /// Testing if we can get least read books.
        /// </summary>
        [Test]
        public void BookListLogic_GetLeastRead()
        {
            this.bookListLogic.Save(new BasicInfos()
            {
                Id = 3,
                Title = "Hogy ne halj meg",
                Author = "Michael Greger - Gene Stone",
                Price = 3990,
                Extras = new Extras()
                {
                    Language = "English",
                    Piece = 2
                },
                Categories = new Categories()
                {
                    Name = "Lifestyle"
                }
            });

            var books = this.bookListLogic.GetLeastRead(2);

            Assert.AreEqual(books.Count, 2);
            Assert.AreEqual(books.ElementAt(0).Id, 1);
            Assert.AreEqual(books.ElementAt(1).Id, 3);
        }
    }
}
