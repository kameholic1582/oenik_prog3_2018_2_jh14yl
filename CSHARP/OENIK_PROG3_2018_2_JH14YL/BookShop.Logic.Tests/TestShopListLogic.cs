﻿// <copyright file="TestShopListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using BookShop.Data;
    using BookShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Testing shop list logic.
    /// </summary>
    [TestFixture]
    public class TestShopListLogic
    {
        private Mock<IRepository<Shops>> mockShopRepo;
        private List<Shops> shops = new List<Shops>();
        private ShopListLogic shopListLogic;

        /// <summary>
        /// Setting up test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.shops = new List<Shops>
            {
                new Shops()
                {
                    Id = 1,
                    City = "Budapest",
                    Address = "1117 Budapest Váli u. 3.",
                    ShopName = "Allee Libri",
                    ShopOwner = "Budai Aladár"
                },
                new Shops()
                {
                    Id = 2,
                    City = "Pécs",
                    Address = "7632 Pécs Megyeri út 76.",
                    ShopName = "Pécs Pláza könyvesbolt",
                    ShopOwner = "Pécsi Pál"
                }
            };
            this.mockShopRepo = new Mock<IRepository<Shops>>();

            this.mockShopRepo.Setup(r => r.All).Returns(this.shops.AsQueryable);
            this.mockShopRepo.Setup(r => r.Save(It.IsAny<Shops>())).Callback(
                (Shops shop) =>
                {
                    var find = this.shops.Find(b => b.Id == shop.Id);
                    if (find != null)
                    {
                        find.Id = shop.Id;
                        find.City = shop.City;
                        find.Address = shop.Address;
                        find.ShopName = shop.ShopName;
                        find.ShopOwner = shop.ShopOwner;
                    }
                    else
                    {
                        this.shops.Add(shop);
                    }
                });
            this.mockShopRepo.Setup(r => r.Delete(It.IsAny<Shops>())).Callback(
                (Shops book) =>
                {
                    this.shops.RemoveAll(b => b.Id == book.Id);
                });

            this.shopListLogic = new ShopListLogic(this.mockShopRepo.Object);
        }

        /// <summary>
        /// Testing if we can get shop details.
        /// </summary>
        [Test]
        public void ShopListLogic_GetAll()
        {
            var shops = this.shopListLogic.Shops;

            Assert.AreEqual(shops.Count(), this.shops.Count);
            Assert.AreEqual(shops.ElementAt(0).ShopName, this.shops.ElementAt(0).ShopName);
            Assert.AreEqual(shops.ElementAt(0).Address, this.shops.ElementAt(0).Address);
            Assert.AreEqual(shops.ElementAt(1).ShopName, this.shops.ElementAt(1).ShopName);
            Assert.AreEqual(shops.ElementAt(1).Address, this.shops.ElementAt(1).Address);
        }

        /// <summary>
        /// Testing if we can delete shops.
        /// </summary>
        [Test]
        public void BookListLogic_Delete()
        {
            this.shopListLogic.Delete(new Shops() { Id = 1 });
            var shops = this.shopListLogic.Shops;

            Assert.AreEqual(shops.Count, 1);
            Assert.AreEqual(shops.ElementAt(0).Id, 2);
        }

        /// <summary>
        /// Testing if we can add shops.
        /// </summary>
        [Test]
        public void BookListLogic_Add()
        {
            this.shopListLogic.Save(new Shops()
            {
                Id = 3,
                City = "city",
                Address = "address",
                ShopName = "name",
                ShopOwner = "owner"
            });
            var shops = this.shopListLogic.Shops;
            Assert.AreEqual(shops.Count, 3);

            var newShop = shops.ElementAt(2);
            Assert.AreEqual(newShop.Id, 3);
            Assert.AreEqual(newShop.City, "city");
            Assert.AreEqual(newShop.Address, "address");
            Assert.AreEqual(newShop.ShopName, "name");
            Assert.AreEqual(newShop.ShopOwner, "owner");
        }

        /// <summary>
        /// Testing if we can modify shops.
        /// </summary>
        [Test]
        public void BookListLogic_Modify()
        {
            this.shopListLogic.Save(new Shops()
            {
                Id = 1,
                City = "modCity",
                Address = "modAddress",
                ShopName = "modName",
                ShopOwner = "modOwner"
            });
            var shops = this.shopListLogic.Shops;
            Assert.AreEqual(shops.Count, 2);

            var modShop = shops.ElementAt(0);
            Assert.AreEqual(modShop.Id, 1);
            Assert.AreEqual(modShop.City, "modCity");
            Assert.AreEqual(modShop.Address, "modAddress");
            Assert.AreEqual(modShop.ShopName, "modName");
            Assert.AreEqual(modShop.ShopOwner, "modOwner");
        }
    }
}
