﻿// <copyright file="TestBookAndCategorySelectionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using BookShop.Data;
    using BookShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Testing book and category selection logics.
    /// </summary>
    [TestFixture]
    public class TestBookAndCategorySelectionLogic
    {
        private List<BasicInfos> books;
        private List<Categories> categories;
        private Mock<IRepository<Categories>> mockCategoriesRepo;
        private BookAndCategorySelectionLogic logic;

        /// <summary>
        /// Setting up test.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.books = new List<BasicInfos>
            {
                new BasicInfos()
                {
                    Id = 1,
                    Title = "Édes emlékek anyukámtól",
                    Author = "Bernáth József",
                    CategoryId = 1
                },
                new BasicInfos()
                {
                    Id = 2,
                    Title = "Tanítsd meg tanulni! - A Lépéselőny tanulási stratégia szülőknek",
                    Author = "Lantos Mihály",
                    CategoryId = 2
                }
            };

            this.categories = new List<Categories>
            {
                new Categories()
                {
                    Id = 1,
                    Name = "Gastronomie",
                    MinimumAge = 12
                },
                new Categories()
                {
                    Id = 2,
                    Name = "Family",
                    MinimumAge = 12
                }
            };

            for (int i = 0; i < 2; ++i)
            {
                this.books.ElementAt(i).Categories = this.categories.ElementAt(i);
            }

            this.mockCategoriesRepo = new Mock<IRepository<Categories>>();

            this.mockCategoriesRepo.Setup(r => r.All).Returns(this.categories.AsQueryable);

            this.logic = new BookAndCategorySelectionLogic(this.mockCategoriesRepo.Object);
        }

        /// <summary>
        /// Test if we can get selectable categories.
        /// </summary>
        [Test]
        public void GetSelectableCategories()
        {
            var categories = this.logic.GetSelectableCategories(this.books.ElementAt(0));

            Assert.AreEqual(categories.Name, this.books.ElementAt(0).Categories.Name);
        }

        /// <summary>
        /// Test if we can get category by its name.
        /// </summary>
        [Test]
        public void GetCategoryByName()
        {
            var category = this.logic.GetCategoryByName("Family");

            Assert.AreEqual(category.Name, "Family");
        }

        /// <summary>
        /// Test if we can get all categories.
        /// </summary>
        [Test]
        public void GetAllCategories()
        {
            var categories = this.logic.AllCategories;

            Assert.AreEqual(categories.Count, 2);
            Assert.AreEqual(categories.ElementAt(0).Name, "Gastronomie");
            Assert.AreEqual(categories.ElementAt(1).Name, "Family");
        }
    }
}
