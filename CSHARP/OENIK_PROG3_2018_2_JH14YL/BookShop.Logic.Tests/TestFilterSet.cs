﻿// <copyright file="TestFilterSet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// Testing filter sets.
    /// </summary>
    [TestFixture]
    public class TestFilterSet
    {
        /// <summary>
        /// Testing proper filters.
        /// </summary>
        [Test]
        public void FilterSet_Ctor_Full()
        {
            FilterSet filters = new FilterSet("English", 50, 100, "Family");

            Assert.AreEqual(filters.LanguageFilter, "English");
            Assert.AreEqual(filters.MinPriceFilter, 50);
            Assert.AreEqual(filters.MaxPriceFilter, 100);
            Assert.AreEqual(filters.CategoriesFilter, "Family");
        }

        /// <summary>
        /// Testing null filters.
        /// </summary>
        [Test]
        public void FilterSet_Ctor_EmptyDefaultValues()
        {
            FilterSet filters = new FilterSet(string.Empty, null, null, string.Empty);

            Assert.AreEqual(filters.LanguageFilter, null);
            Assert.AreEqual(filters.MinPriceFilter, uint.MinValue);
            Assert.AreEqual(filters.MaxPriceFilter, uint.MaxValue);
            Assert.AreEqual(filters.CategoriesFilter, null);
        }
    }
}
