﻿CREATE TABLE [dbo].[BasicInfos] (
    [Id]          INT            NOT NULL,
    [Title]       NVARCHAR (MAX) NOT NULL,
    [Author]      NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [Price]       MONEY          NOT NULL,
    [PictureUrl]  NVARCHAR (MAX) NULL,
    [CategoryId]  INT            NOT NULL,
    [ExtrasId]    INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BasicInfos_ToCategories] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Categories] ([Id]),
    CONSTRAINT [FK_BasicInfos_ToExtras] FOREIGN KEY ([ExtrasId]) REFERENCES [dbo].[Extras] ([Id])
);

