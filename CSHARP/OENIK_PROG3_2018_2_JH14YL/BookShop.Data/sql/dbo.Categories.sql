﻿CREATE TABLE [dbo].[Categories]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [MinimumAge] INT NOT NULL
)





INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (1, N'Family', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (2, N'Lifestyle', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (3, N'Biography', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (4, N'Ezotery', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (5, N'Gastronomie', 12)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (6, N'Children', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (7, N'Hobby', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (8, N'Literature', 6)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (9, N'Comics', 10)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (10, N'Garden', 6)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (11, N'Art', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (12, N'Languages', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (13, N'Sport', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (14, N'Informatics', 6)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (15, N'History', 6)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (16, N'Travelling', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (17, N'Religion', 8)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (18, N'Nature', 4)
INSERT INTO [dbo].[Categories] ([Id], [Name], [MinimumAge]) VALUES (19, N'Foreign language', 4)
