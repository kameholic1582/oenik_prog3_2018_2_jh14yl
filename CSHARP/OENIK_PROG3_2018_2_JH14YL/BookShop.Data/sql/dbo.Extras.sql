﻿CREATE TABLE [dbo].[Extras]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Language] NVARCHAR(50) NOT NULL, 
    [Place] NVARCHAR(MAX) NOT NULL, 
    [Piece] INT NULL
)




INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (1, 1, N'Hungarian', 3)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (2, 2, N'Hungarian', 17)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (3, 3, N'Hungarian', 1)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (4, 4, N'Hungarian', 2)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (5, 5, N'Hungarian', 10)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (6, 6, N'Hungarian', 23)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (7, 7, N'Hungarian', 7)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (8, 8, N'Hungarian', 1)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (9, 9, N'Hungarian', 8)
INSERT INTO [dbo].[Extras] ([Id], [BookId], [Language], [Piece]) VALUES (10, 10, N'English', 30)
