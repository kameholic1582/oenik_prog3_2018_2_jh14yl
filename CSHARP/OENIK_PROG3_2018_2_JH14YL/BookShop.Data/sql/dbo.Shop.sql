﻿CREATE TABLE [dbo].[Shop]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [City] NVARCHAR(50) NOT NULL, 
    [ShopName] NVARCHAR(50) NOT NULL, 
    [ShopOwner] NVARCHAR(50) NOT NULL, 
    [BasicInfosId] INT NOT NULL
)




INSERT INTO [dbo].[Shop] ([Id], [City], [Address], [ShopName], [ShopOwner]) VALUES (1, N'Budapest', N'1117 Budapest Váli u. 3.', N'Allee Libri', N'Budai Aladár')
INSERT INTO [dbo].[Shop] ([Id], [City], [Address], [ShopName], [ShopOwner]) VALUES (2, N'Pécs', N'7632 Pécs Megyeri út 76.', N'Pécs Pláza könyvesbolt', N'Pécsi Pál')
INSERT INTO [dbo].[Shop] ([Id], [City], [Address], [ShopName], [ShopOwner]) VALUES (3, N'Szeged', N'6724 Szeged Kossuth Lajos sgt. 119.', N'Szeged Pláza könyvesbolt', N'Szegedi Péter')
INSERT INTO [dbo].[Shop] ([Id], [City], [Address], [ShopName], [ShopOwner]) VALUES (4, N'Kecskemét', N'6000 Kecskemét Korona u. 2', N'Kecskemét Malom könyvesbolt', N'Kecskeméti Mária')
INSERT INTO [dbo].[Shop] ([Id], [City], [Address], [ShopName], [ShopOwner]) VALUES (5, N'Veszprém', N'8200 Veszprém Budapesti út 20-28.', N'Balaton Pláza', N'Balatoni Veronika')
