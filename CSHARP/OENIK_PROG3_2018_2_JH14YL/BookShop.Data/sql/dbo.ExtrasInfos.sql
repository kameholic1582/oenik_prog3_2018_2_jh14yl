﻿CREATE TABLE [dbo].[ExtrasInfos]
(
	[ShopId] INT NOT NULL, 
    [ExtraId] INT NOT NULL,
	CONSTRAINT [PK_ExtrasInfos] PRIMARY KEY CLUSTERED ([ExtraId] ASC, [ExtraId] ASC),

)




INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (1, 1, 1)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (2, 1, 5)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (3, 2, 4)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (4, 2, 7)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (5, 2, 2)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (6, 3, 8)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (7, 4, 3)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (8, 4, 6)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (9, 5, 9)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (10, 5, 10)
INSERT INTO [dbo].[ExtrasInfos] ([ID], [ShopID], [BasicInfoId]) VALUES (11, 5, 11)
