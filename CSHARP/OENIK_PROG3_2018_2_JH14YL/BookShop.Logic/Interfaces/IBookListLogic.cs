﻿// <copyright file="IBookListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Business logic for getting books that are filtered or last read.
    /// Saving and deleting book.
    /// Get book by its id.
    /// </summary>
    public interface IBookListLogic
    {
        /// <summary>
        /// Gets the books that are filtered.
        /// </summary>
        /// <param name="filters">Filters.</param>
        /// <returns>A BasicInfos which is basically a book.</returns>
        IList<BasicInfos> GetFilteredBookList(FilterSet filters);

        /// <summary>
        /// Gets the n least read books.
        /// </summary>
        /// <param name="n">Number of last read books the user want to get back.</param>
        /// <returns>List of books.</returns>
        IList<BasicInfos> GetLeastRead(int n);

        /// <summary>
        /// Deleting a book.
        /// </summary>
        /// <param name="book">The book I want to delete.</param>
        void Delete(BasicInfos book);

        /// <summary>
        /// Saving a book.
        /// </summary>
        /// <param name="book">The book I want to save.</param>
        void Save(BasicInfos book);

        /// <summary>
        /// Getting a book by its id.
        /// </summary>
        /// <param name="id">The book I want to get by its id.</param>
        /// <returns>Book.</returns>
        BasicInfos GetBookById(int id);
    }
}
