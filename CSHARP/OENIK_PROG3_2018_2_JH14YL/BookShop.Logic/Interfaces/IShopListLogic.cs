﻿// <copyright file="IShopListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Business logic for shops (deleting, save and get them).
    /// </summary>
    public interface IShopListLogic
    {
        /// <summary>
        /// Gets the shops.
        /// </summary>
        IList<Shops> Shops { get; }

        /// <summary>
        /// Deleting shop.
        /// </summary>
        /// <param name="shop">Shop</param>
        void Delete(Shops shop);

        /// <summary>
        /// Saving shop.
        /// </summary>
        /// <param name="shop">Shop</param>
        void Save(Shops shop);

        /// <summary>
        /// Get shop by its id.
        /// </summary>
        /// <param name="id">Shops id.</param>
        /// <returns>Shop</returns>
        Shops GetShopById(int id);
    }
}
