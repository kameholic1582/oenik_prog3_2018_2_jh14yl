﻿// <copyright file="IBookAndCategorySelectionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Business logic for getting books that are selectable if a Category is selected;
    /// or selectable categories when a book is selected.
    /// </summary>
    public interface IBookAndCategorySelectionLogic
    {
        /// <summary>
        /// Gets the list of the categories.
        /// </summary>
        IList<Categories> AllCategories { get; }

        /// <summary>
        /// Returns the categories that are selected.
        /// </summary>
        /// <param name="selected">Selected book</param>
        /// <returns>Categories</returns>
        Categories GetSelectableCategories(BasicInfos selected);

        /// <summary>
        /// Gets the category by its name.
        /// </summary>
        /// <param name="name">The name of the category</param>
        /// <returns>Categories</returns>
        Categories GetCategoryByName(string name);
    }
}
