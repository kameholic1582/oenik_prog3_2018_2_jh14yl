﻿// <copyright file="ShopListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;
    using BookShop.Logic.Interfaces;
    using BookShop.Repository;

    /// <summary>
    /// Business logic for the shops.
    /// </summary>
    public class ShopListLogic : IShopListLogic
    {
        /// <summary>
        /// Repository of shops.
        /// </summary>
        private readonly IRepository<Shops> shopRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopListLogic"/> class.
        /// Setting shop repository.
        /// </summary>
        /// <param name="shopRepository">Repository of shops.</param>
        public ShopListLogic(IRepository<Shops> shopRepository)
        {
            this.shopRepository = shopRepository;
        }

        /// <summary>
        /// Event if the shop list changes.
        /// </summary>
        public event EventHandler ShopListChanged;

        /// <summary>
        /// Gets a list of shops.
        /// </summary>
        public IList<Shops> Shops => this.shopRepository.All.ToList();

        /// <summary>
        /// Gets the shop by its id.
        /// </summary>
        /// <param name="id">Id of the shop.</param>
        /// <returns>Shop.</returns>
        public Shops GetShopById(int id)
        {
            return this.shopRepository.All.Where(s => s.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Deleting shop.
        /// </summary>
        /// <param name="shop">Shop.</param>
        public void Delete(Shops shop)
        {
            try
            {
                this.shopRepository.Delete(shop);
            }
            finally
            {
                this.OnShopListChanged();
            }
        }

        /// <summary>
        /// Saving shop.
        /// </summary>
        /// <param name="shop">Shop.</param>
        public void Save(Shops shop)
        {
            try
            {
                this.shopRepository.Save(shop);
            }
            finally
            {
                this.OnShopListChanged();
            }
        }

        /// <summary>
        /// Invokes an event if the list of shops changed.
        /// </summary>
        private void OnShopListChanged()
        {
            this.ShopListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
