﻿// <copyright file="FilterSet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;

    /// <summary>
    /// Filters.
    /// </summary>
    public class FilterSet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterSet"/> class.
        /// Setting filters.
        /// </summary>
        /// <param name="languageFilter">Language filter.</param>
        /// <param name="minPriceFilter">Minimum price filter.</param>
        /// <param name="maxPriceFilter">Maximum price filter.</param>
        /// <param name="categoriesFilter">Category filter.</param>
        public FilterSet(string languageFilter, uint? minPriceFilter, uint? maxPriceFilter, string categoriesFilter)
        {
            this.LanguageFilter = string.IsNullOrEmpty(languageFilter) ? null : languageFilter;
            this.MinPriceFilter = minPriceFilter ?? uint.MinValue;
            this.MaxPriceFilter = maxPriceFilter ?? uint.MaxValue;
            this.CategoriesFilter = string.IsNullOrEmpty(categoriesFilter) ? null : categoriesFilter;
        }

        /// <summary>
        /// Gets or sets the language filter.
        /// </summary>
        public string LanguageFilter { get; set; }

        /// <summary>
        /// Gets or sets the minimum price filter.
        /// </summary>
        public uint MinPriceFilter { get; set; }

        /// <summary>
        /// Gets or sets the maximum price filter.
        /// </summary>
        public uint MaxPriceFilter { get; set; }

        /// <summary>
        /// Gets or sets the category filter.
        /// </summary>
        public string CategoriesFilter { get; set; }
    }
}
