﻿// <copyright file="BookListLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;
    using BookShop.Logic.Interfaces;
    using BookShop.Repository;

    /// <summary>
    /// Business logic for books.
    /// </summary>
    public class BookListLogic : IBookListLogic
    {
        /// <summary>
        /// Repository for books.
        /// </summary>
        private readonly IRepository<BasicInfos> bookRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookListLogic"/> class.
        /// Setting BookRepository.
        /// </summary>
        /// <param name="bookRepository">Book repository.</param>
        public BookListLogic(IRepository<BasicInfos> bookRepository)
        {
            this.bookRepository = bookRepository;
        }

        /// <summary>
        /// Event for book changes.
        /// </summary>
        public event EventHandler BookListChanged;

        /// <summary>
        /// Deleting book.
        /// </summary>
        /// <param name="book">Book to delete.</param>
        public void Delete(BasicInfos book)
        {
            try
            {
                this.bookRepository.Delete(book);
            }
            finally
            {
                this.OnBookListChanged();
            }
        }

        /// <summary>
        /// Gets book by its id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>A book and its infos.</returns>
        public BasicInfos GetBookById(int id)
        {
            return this.bookRepository.All.Where(b => b.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Gets the list of the books that are true for the filter.
        /// </summary>
        /// <param name="filters">The filter I use.</param>
        /// <returns>A list of books.</returns>
        public IList<BasicInfos> GetFilteredBookList(FilterSet filters)
        {
            var basicInfos = this.bookRepository.All;
            if (filters == null)
            {
                return basicInfos.ToList();
            }

            if (filters.CategoriesFilter != null)
            {
                basicInfos = basicInfos.Where(m => m.Categories.Name.StartsWith(filters.CategoriesFilter));
            }

            if (filters.LanguageFilter != null)
            {
                basicInfos = basicInfos.Where(m => m.Extras.Language.StartsWith(filters.LanguageFilter));
            }

            if (filters.MinPriceFilter != uint.MinValue || filters.MaxPriceFilter != uint.MaxValue)
            {
                basicInfos = basicInfos.Where(m =>
                         m.Price >= filters.MinPriceFilter &&
                         m.Price <= filters.MaxPriceFilter);
            }

            return basicInfos.ToList();
        }

        /// <summary>
        /// Gets the n least read books.
        /// </summary>
        /// <param name="n">Number you want to get back.</param>
        /// <returns>A list of books.</returns>
        public IList<BasicInfos> GetLeastRead(int n)
        {
            return this.bookRepository.All.OrderByDescending(b => b.Extras.Piece).Take(n).ToList();
        }

        /// <summary>
        /// Saving books.
        /// </summary>
        /// <param name="book">Book to save.</param>
        public void Save(BasicInfos book)
        {
            try
            {
                this.bookRepository.Save(book);
            }
            finally
            {
                this.OnBookListChanged();
            }
        }

        /// <summary>
        /// Invokes an event if the list of books changed.
        /// </summary>
        private void OnBookListChanged()
        {
            this.BookListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
