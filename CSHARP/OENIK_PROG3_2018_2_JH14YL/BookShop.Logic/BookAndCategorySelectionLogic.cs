﻿// <copyright file="BookAndCategorySelectionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BookShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShop.Data;
    using BookShop.Logic.Interfaces;
    using BookShop.Repository;

    /// <summary>
    /// Business logic for book and category selections.
    /// </summary>
    public class BookAndCategorySelectionLogic : IBookAndCategorySelectionLogic
    {
        /// <summary>
        /// Repository of categories.
        /// </summary>
        private readonly IRepository<Categories> categoriesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookAndCategorySelectionLogic"/> class.
        /// Setting categoriesRepository.
        /// </summary>
        /// <param name="categoriesRepository">Categories' repository.</param>
        public BookAndCategorySelectionLogic(
            IRepository<Categories> categoriesRepository)
        {
            this.categoriesRepository = categoriesRepository;
        }

        /// <summary>
        /// Gets the list of the categories.
        /// </summary>
        public IList<Categories> AllCategories => this.categoriesRepository.All.ToList();

        /// <summary>
        /// Get the selected book's category.
        /// </summary>
        /// <param name="selected">Book.</param>
        /// <returns>Category of the book.</returns>
        public Categories GetSelectableCategories(BasicInfos selected)
        {
            if (selected == null)
            {
                return null;
            }
            else
            {
                return selected.Categories;
            }
        }

        /// <summary>
        /// Gets the name of the category.
        /// </summary>
        /// <param name="name">Category name.</param>
        /// <returns>category.</returns>
        public Categories GetCategoryByName(string name)
        {
            return this.categoriesRepository.All.Where(c => c.Name.Equals(name)).FirstOrDefault();
        }
    }
}
