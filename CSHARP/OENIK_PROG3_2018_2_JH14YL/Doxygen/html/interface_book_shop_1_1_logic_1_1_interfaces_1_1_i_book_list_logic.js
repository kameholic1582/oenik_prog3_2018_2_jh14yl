var interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic =
[
    [ "Delete", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html#a88ce13edfb362dc34e3886837c096de3", null ],
    [ "GetBookById", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html#a7211ac79e836fec5d3f3e5c411055a4f", null ],
    [ "GetFilteredBookList", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html#a618750047639f3698f878926a1a21855", null ],
    [ "GetLeastRead", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html#a04d01830b17d3e07ec6942a3b8326297", null ],
    [ "Save", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html#a1ebdfa3ac1b2e6c54679551a1990f77a", null ]
];