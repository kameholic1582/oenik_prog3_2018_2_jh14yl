var dir_124994fdef9785bfa269aa30ee5caea5 =
[
    [ "BookShop.Data", "dir_60da20ce3d1c16853a6cd9c9187920ae.html", "dir_60da20ce3d1c16853a6cd9c9187920ae" ],
    [ "BookShop.Logic", "dir_1b08807bb4c4dfe2c428176e2abde035.html", "dir_1b08807bb4c4dfe2c428176e2abde035" ],
    [ "BookShop.Logic.Tests", "dir_02d0eec87bef6913abc24ac6ec415be6.html", "dir_02d0eec87bef6913abc24ac6ec415be6" ],
    [ "BookShop.Program", "dir_64d786ccde1a3cdb8f18c1c74f7902c2.html", "dir_64d786ccde1a3cdb8f18c1c74f7902c2" ],
    [ "BookShop.Repository", "dir_5988062c39b249e56e9eaad017c367ef.html", "dir_5988062c39b249e56e9eaad017c367ef" ],
    [ "BookShop.Repository.Tests", "dir_e42fa85e600909b164c56b05c441d1f4.html", "dir_e42fa85e600909b164c56b05c441d1f4" ]
];