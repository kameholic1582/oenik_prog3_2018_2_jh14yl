var namespace_book_shop =
[
    [ "Data", "namespace_book_shop_1_1_data.html", "namespace_book_shop_1_1_data" ],
    [ "Logic", "namespace_book_shop_1_1_logic.html", "namespace_book_shop_1_1_logic" ],
    [ "Program", "namespace_book_shop_1_1_program.html", "namespace_book_shop_1_1_program" ],
    [ "Repository", "namespace_book_shop_1_1_repository.html", "namespace_book_shop_1_1_repository" ]
];