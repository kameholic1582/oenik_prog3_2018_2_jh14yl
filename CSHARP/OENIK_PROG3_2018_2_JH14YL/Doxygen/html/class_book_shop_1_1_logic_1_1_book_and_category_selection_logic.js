var class_book_shop_1_1_logic_1_1_book_and_category_selection_logic =
[
    [ "BookAndCategorySelectionLogic", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html#ae8e2606694da65c217c29aca59a6410f", null ],
    [ "GetCategoryByName", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html#a7fa7f7d945653140846a5a64a4d26d4a", null ],
    [ "GetSelectableCategories", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html#ada7924aac106cad6d87328f734a8ae4d", null ],
    [ "AllCategories", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html#aae9935402a41bbd970faa2e97d22f333", null ]
];