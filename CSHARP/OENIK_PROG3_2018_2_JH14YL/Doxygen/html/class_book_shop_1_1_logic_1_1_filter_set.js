var class_book_shop_1_1_logic_1_1_filter_set =
[
    [ "FilterSet", "class_book_shop_1_1_logic_1_1_filter_set.html#a471c806ce91bb62c5aec9385d0566c07", null ],
    [ "CategoriesFilter", "class_book_shop_1_1_logic_1_1_filter_set.html#a29eed383ce145b04ded154f8a3ad7e20", null ],
    [ "LanguageFilter", "class_book_shop_1_1_logic_1_1_filter_set.html#ac7548304819927bd63cb90eb52657b9b", null ],
    [ "MaxPriceFilter", "class_book_shop_1_1_logic_1_1_filter_set.html#aafb18e0b2947d3a5eae30ba43170c00d", null ],
    [ "MinPriceFilter", "class_book_shop_1_1_logic_1_1_filter_set.html#a7fc4607aea910beada81ee368fe1cb40", null ]
];