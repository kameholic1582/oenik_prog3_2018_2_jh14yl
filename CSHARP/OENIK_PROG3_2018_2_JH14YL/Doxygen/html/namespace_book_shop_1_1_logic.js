var namespace_book_shop_1_1_logic =
[
    [ "Interfaces", "namespace_book_shop_1_1_logic_1_1_interfaces.html", "namespace_book_shop_1_1_logic_1_1_interfaces" ],
    [ "Tests", "namespace_book_shop_1_1_logic_1_1_tests.html", "namespace_book_shop_1_1_logic_1_1_tests" ],
    [ "BookAndCategorySelectionLogic", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic" ],
    [ "BookListLogic", "class_book_shop_1_1_logic_1_1_book_list_logic.html", "class_book_shop_1_1_logic_1_1_book_list_logic" ],
    [ "FilterSet", "class_book_shop_1_1_logic_1_1_filter_set.html", "class_book_shop_1_1_logic_1_1_filter_set" ],
    [ "ShopListLogic", "class_book_shop_1_1_logic_1_1_shop_list_logic.html", "class_book_shop_1_1_logic_1_1_shop_list_logic" ]
];