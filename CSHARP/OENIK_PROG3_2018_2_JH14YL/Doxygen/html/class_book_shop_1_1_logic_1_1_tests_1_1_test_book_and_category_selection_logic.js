var class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic =
[
    [ "GetAllCategories", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html#a7775ec70ee16978e0f48b696f42f0368", null ],
    [ "GetCategoryByName", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html#ab0e8b1a9a671cf1dfef08571458746f8", null ],
    [ "GetSelectableCategories", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html#a21f75cafbea0abe63bc246234b748e68", null ],
    [ "SetUp", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html#a03ba251c153f8c766a722e9c3fb96648", null ]
];