var class_book_shop_1_1_logic_1_1_shop_list_logic =
[
    [ "ShopListLogic", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#ab6b066f8b2048cbb5778d8b081a656ee", null ],
    [ "Delete", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#ad9680a6dc1df646000b3474487c27b82", null ],
    [ "GetShopById", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#af74f55e235119a538e0e3abaaea84237", null ],
    [ "Save", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#aa79889e4b2f905ef822ec315833a91db", null ],
    [ "Shops", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#a9ac18c9ef26d57caebedadabde799c59", null ],
    [ "ShopListChanged", "class_book_shop_1_1_logic_1_1_shop_list_logic.html#aeffe2313118a7d30fac337651d8e3c02", null ]
];