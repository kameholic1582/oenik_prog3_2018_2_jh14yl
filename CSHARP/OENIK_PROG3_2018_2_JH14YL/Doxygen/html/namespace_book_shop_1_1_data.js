var namespace_book_shop_1_1_data =
[
    [ "BasicInfos", "class_book_shop_1_1_data_1_1_basic_infos.html", "class_book_shop_1_1_data_1_1_basic_infos" ],
    [ "BookShopDBEntities", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities" ],
    [ "Categories", "class_book_shop_1_1_data_1_1_categories.html", "class_book_shop_1_1_data_1_1_categories" ],
    [ "Extras", "class_book_shop_1_1_data_1_1_extras.html", "class_book_shop_1_1_data_1_1_extras" ],
    [ "ExtrasInfos", "class_book_shop_1_1_data_1_1_extras_infos.html", "class_book_shop_1_1_data_1_1_extras_infos" ],
    [ "Shops", "class_book_shop_1_1_data_1_1_shops.html", "class_book_shop_1_1_data_1_1_shops" ]
];