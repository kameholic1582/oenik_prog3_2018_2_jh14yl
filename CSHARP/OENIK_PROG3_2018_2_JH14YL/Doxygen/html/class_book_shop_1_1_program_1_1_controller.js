var class_book_shop_1_1_program_1_1_controller =
[
    [ "Controller", "class_book_shop_1_1_program_1_1_controller.html#a9963188273c0e066b912d9352f0e1508", null ],
    [ "AddBook", "class_book_shop_1_1_program_1_1_controller.html#a56ab45c486a49478750ee2d07a40adaf", null ],
    [ "AddShop", "class_book_shop_1_1_program_1_1_controller.html#a1ffe968feea4e6b16fa57f85bfcea8f3", null ],
    [ "BookList", "class_book_shop_1_1_program_1_1_controller.html#a55a43bbd1a11d83a0f9862516814dffb", null ],
    [ "DeleteBook", "class_book_shop_1_1_program_1_1_controller.html#a3cd794b1a69b0b74d544742dcd8af9cb", null ],
    [ "DeleteShop", "class_book_shop_1_1_program_1_1_controller.html#aa1f27e7d06ad0e40601dea71f872b844", null ],
    [ "FilterBooks", "class_book_shop_1_1_program_1_1_controller.html#a50e878ac71a9ee9919bd801f227e7f24", null ],
    [ "LeastReadBooks", "class_book_shop_1_1_program_1_1_controller.html#aff26c4c0718cd8946d497e9b09634823", null ],
    [ "ModifyBook", "class_book_shop_1_1_program_1_1_controller.html#a6e29679647c877bf4446ff2723781ed3", null ],
    [ "ModifyShop", "class_book_shop_1_1_program_1_1_controller.html#a9add774d88fc974f4ceed8ba1395a230", null ],
    [ "ShopList", "class_book_shop_1_1_program_1_1_controller.html#afc14fc4413e5948a0c35bd832ef28592", null ],
    [ "ShopsOnGoogleMaps", "class_book_shop_1_1_program_1_1_controller.html#a88248fbbf0be5789f5dd9623d86179fa", null ]
];