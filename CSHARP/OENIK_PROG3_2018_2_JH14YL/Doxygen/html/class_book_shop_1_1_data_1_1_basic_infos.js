var class_book_shop_1_1_data_1_1_basic_infos =
[
    [ "BasicInfos", "class_book_shop_1_1_data_1_1_basic_infos.html#a98a56f8efa9c61c38db225141eecbf96", null ],
    [ "ToString", "class_book_shop_1_1_data_1_1_basic_infos.html#af26823c5f31405de03d81870ffe56df8", null ],
    [ "Author", "class_book_shop_1_1_data_1_1_basic_infos.html#acbee706655646f87bcab9c7cfa0b46c6", null ],
    [ "Categories", "class_book_shop_1_1_data_1_1_basic_infos.html#a153d433c5cb43f2191a2afb5d448ae96", null ],
    [ "CategoryId", "class_book_shop_1_1_data_1_1_basic_infos.html#a5a29615fa3adf398ea9eacf8530a952a", null ],
    [ "Description", "class_book_shop_1_1_data_1_1_basic_infos.html#a145e16f3e8bfc16999d95265f6be0918", null ],
    [ "Extras", "class_book_shop_1_1_data_1_1_basic_infos.html#a0764de1c41aa3069dc44aeb2894c06e3", null ],
    [ "ExtrasId", "class_book_shop_1_1_data_1_1_basic_infos.html#ac50eade8e6054d7c154852793a039f32", null ],
    [ "ExtrasInfos", "class_book_shop_1_1_data_1_1_basic_infos.html#a1d2003d92b3f75daec2da5bff4ddef9c", null ],
    [ "Id", "class_book_shop_1_1_data_1_1_basic_infos.html#aa86d7e9378c48ad626f61c09e307adc4", null ],
    [ "PictureUrl", "class_book_shop_1_1_data_1_1_basic_infos.html#aa1c557a066156b87db8c2f5f4274d8ee", null ],
    [ "Price", "class_book_shop_1_1_data_1_1_basic_infos.html#a4bcea22e22e82e34ede058d018133487", null ],
    [ "Title", "class_book_shop_1_1_data_1_1_basic_infos.html#a64a6281430844a4bc6ab61940f963f56", null ]
];