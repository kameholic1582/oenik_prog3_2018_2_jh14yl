var namespace_book_shop_1_1_logic_1_1_interfaces =
[
    [ "IBookAndCategorySelectionLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic" ],
    [ "IBookListLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic" ],
    [ "IShopListLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_shop_list_logic.html", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_shop_list_logic" ]
];