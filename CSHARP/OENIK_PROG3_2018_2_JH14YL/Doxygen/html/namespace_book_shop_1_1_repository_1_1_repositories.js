var namespace_book_shop_1_1_repository_1_1_repositories =
[
    [ "BasicInfosRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_basic_infos_repository.html", "class_book_shop_1_1_repository_1_1_repositories_1_1_basic_infos_repository" ],
    [ "CategoriesRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_categories_repository.html", "class_book_shop_1_1_repository_1_1_repositories_1_1_categories_repository" ],
    [ "ExtrasRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_extras_repository.html", "class_book_shop_1_1_repository_1_1_repositories_1_1_extras_repository" ],
    [ "ShopRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_shop_repository.html", "class_book_shop_1_1_repository_1_1_repositories_1_1_shop_repository" ]
];