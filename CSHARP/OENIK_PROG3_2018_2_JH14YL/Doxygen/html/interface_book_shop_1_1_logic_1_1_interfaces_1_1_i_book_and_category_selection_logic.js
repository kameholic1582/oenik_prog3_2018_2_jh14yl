var interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic =
[
    [ "GetCategoryByName", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html#aa13773965c6064d30d1fb5e0f2ad1e34", null ],
    [ "GetSelectableCategories", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html#a284575ce37774da0695a225a12479686", null ],
    [ "AllCategories", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html#a46c42d05edf7ab57510063f186a8a6ef", null ]
];