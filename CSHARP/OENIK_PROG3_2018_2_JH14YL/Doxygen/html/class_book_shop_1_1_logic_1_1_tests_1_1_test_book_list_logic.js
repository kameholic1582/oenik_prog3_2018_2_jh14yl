var class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic =
[
    [ "BookListLogic_Add", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#ab8ddcec63d192470152e7162a944be9c", null ],
    [ "BookListLogic_Delete", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#ae8daac94c8b8bc8cfd606986d98843e9", null ],
    [ "BookListLogic_GetAll", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#a98cc4e74be7ec7e797377bc988b0b048", null ],
    [ "BookListLogic_GetBookById", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#a9f39e1d3ad72d9f9e0e09b92ec9549d7", null ],
    [ "BookListLogic_GetLeastRead", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#aa30614ead075b65fbc2bc61f2abe1b23", null ],
    [ "BookListLogic_GetWithCategoryFilters", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#a4ac1b5fcb65e08591b30b180233030d4", null ],
    [ "BookListLogic_GetWithEmptyFilters", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#aedb89e7ede44e700530812def86cb958", null ],
    [ "BookListLogic_GetWithLanguageFilters", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#ae0e296e1e461a00f14f9005fb33af98b", null ],
    [ "BookListLogic_GetWithPriceFilters", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#aaee46b03f5c7ea58aaa696a196649959", null ],
    [ "BookListLogic_Modify", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#a4afe513c97e69bee56b4be1d16594b4a", null ],
    [ "SetUp", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html#ad5959e693593bab0b0266a01b78f4d99", null ]
];