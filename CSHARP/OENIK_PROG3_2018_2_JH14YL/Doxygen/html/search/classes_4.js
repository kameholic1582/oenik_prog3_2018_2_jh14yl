var searchData=
[
  ['ibookandcategoryselectionlogic',['IBookAndCategorySelectionLogic',['../interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html',1,'BookShop::Logic::Interfaces']]],
  ['ibooklistlogic',['IBookListLogic',['../interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html',1,'BookShop::Logic::Interfaces']]],
  ['irepository',['IRepository',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20basicinfos_20_3e',['IRepository&lt; BasicInfos &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20bookshop_3a_3adata_3a_3abasicinfos_20_3e',['IRepository&lt; BookShop::Data::BasicInfos &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20bookshop_3a_3adata_3a_3acategories_20_3e',['IRepository&lt; BookShop::Data::Categories &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20bookshop_3a_3adata_3a_3ashops_20_3e',['IRepository&lt; BookShop::Data::Shops &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20categories_20_3e',['IRepository&lt; Categories &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20extras_20_3e',['IRepository&lt; Extras &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['irepository_3c_20shops_20_3e',['IRepository&lt; Shops &gt;',['../interface_book_shop_1_1_repository_1_1_i_repository.html',1,'BookShop::Repository']]],
  ['ishoplistlogic',['IShopListLogic',['../interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_shop_list_logic.html',1,'BookShop::Logic::Interfaces']]]
];
