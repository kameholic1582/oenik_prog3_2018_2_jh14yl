var searchData=
[
  ['all',['All',['../class_book_shop_1_1_repository_1_1_repositories_1_1_basic_infos_repository.html#a11150bb4bb5987c9bcd02c61d1ab3122',1,'BookShop.Repository.Repositories.BasicInfosRepository.All()'],['../class_book_shop_1_1_repository_1_1_repositories_1_1_categories_repository.html#a20dac7839309c0ee555a41211ddaa2d0',1,'BookShop.Repository.Repositories.CategoriesRepository.All()'],['../class_book_shop_1_1_repository_1_1_repositories_1_1_extras_repository.html#a6b54660e4c4ce3aa6acc01a847cb9a9a',1,'BookShop.Repository.Repositories.ExtrasRepository.All()'],['../class_book_shop_1_1_repository_1_1_repositories_1_1_shop_repository.html#a28761d7cdffd4a1624dc7f1ec968f305',1,'BookShop.Repository.Repositories.ShopRepository.All()']]],
  ['allcategories',['AllCategories',['../class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html#aae9935402a41bbd970faa2e97d22f333',1,'BookShop::Logic::BookAndCategorySelectionLogic']]]
];
