var indexSectionsWithContent =
{
  0: "abcdefgilmnrst",
  1: "bcefilst",
  2: "b",
  3: "abcdefglmrs",
  4: "as",
  5: "aclms",
  6: "bs",
  7: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events",
  7: "Pages"
};

