var namespace_book_shop_1_1_logic_1_1_tests =
[
    [ "TestBookAndCategorySelectionLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic" ],
    [ "TestBookListLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic" ],
    [ "TestFilterSet", "class_book_shop_1_1_logic_1_1_tests_1_1_test_filter_set.html", "class_book_shop_1_1_logic_1_1_tests_1_1_test_filter_set" ],
    [ "TestShopListLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_shop_list_logic.html", "class_book_shop_1_1_logic_1_1_tests_1_1_test_shop_list_logic" ]
];