var class_book_shop_1_1_logic_1_1_book_list_logic =
[
    [ "BookListLogic", "class_book_shop_1_1_logic_1_1_book_list_logic.html#ace2dd9a7a79a93e58e64e8d602e51525", null ],
    [ "Delete", "class_book_shop_1_1_logic_1_1_book_list_logic.html#ab53b1e4dc406b8a85ce2c0d58051d331", null ],
    [ "GetBookById", "class_book_shop_1_1_logic_1_1_book_list_logic.html#ab4c96f6a7f16f66a0a8ae9ada86e86df", null ],
    [ "GetFilteredBookList", "class_book_shop_1_1_logic_1_1_book_list_logic.html#a682141f40fab4dac1b151d06fa511bec", null ],
    [ "GetLeastRead", "class_book_shop_1_1_logic_1_1_book_list_logic.html#ae557f2ae747f38c93bf664775a19b310", null ],
    [ "Save", "class_book_shop_1_1_logic_1_1_book_list_logic.html#a2063b8534118a6d6d7ef7706750a8592", null ],
    [ "BookListChanged", "class_book_shop_1_1_logic_1_1_book_list_logic.html#ab8efd7f5c5d4a1a53f7bc986cde1260e", null ]
];