var namespace_book_shop_1_1_program_1_1_maps_service_reference =
[
    [ "BookShopWebService", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service.html", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service" ],
    [ "BookShopWebServiceChannel", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_channel.html", null ],
    [ "BookShopWebServiceClient", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_client.html", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_client" ],
    [ "locationRequest", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request.html", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request" ],
    [ "locationRequestBody", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request_body.html", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request_body" ],
    [ "locationResponse", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response.html", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response" ],
    [ "locationResponseBody", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response_body.html", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response_body" ]
];