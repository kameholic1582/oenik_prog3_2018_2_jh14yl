var hierarchy =
[
    [ "BookShop.Data.BasicInfos", "class_book_shop_1_1_data_1_1_basic_infos.html", null ],
    [ "BookShop.Program.MapsServiceReference.BookShopWebService", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service.html", [
      [ "BookShop.Program.MapsServiceReference.BookShopWebServiceChannel", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_channel.html", null ],
      [ "BookShop.Program.MapsServiceReference.BookShopWebServiceClient", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_client.html", null ]
    ] ],
    [ "BookShop.Data.Categories", "class_book_shop_1_1_data_1_1_categories.html", null ],
    [ "ClientBase", null, [
      [ "BookShop.Program.MapsServiceReference.BookShopWebServiceClient", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_client.html", null ]
    ] ],
    [ "BookShop.Program.ConsoleMenu.ConsoleManager", "class_book_shop_1_1_program_1_1_console_menu_1_1_console_manager.html", null ],
    [ "BookShop.Program.Controller", "class_book_shop_1_1_program_1_1_controller.html", null ],
    [ "DbContext", null, [
      [ "BookShop.Data.BookShopDBEntities", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html", null ]
    ] ],
    [ "BookShop.Data.Extras", "class_book_shop_1_1_data_1_1_extras.html", null ],
    [ "BookShop.Data.ExtrasInfos", "class_book_shop_1_1_data_1_1_extras_infos.html", null ],
    [ "BookShop.Logic.FilterSet", "class_book_shop_1_1_logic_1_1_filter_set.html", null ],
    [ "BookShop.Logic.Interfaces.IBookAndCategorySelectionLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_and_category_selection_logic.html", [
      [ "BookShop.Logic.BookAndCategorySelectionLogic", "class_book_shop_1_1_logic_1_1_book_and_category_selection_logic.html", null ]
    ] ],
    [ "BookShop.Logic.Interfaces.IBookListLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_book_list_logic.html", [
      [ "BookShop.Logic.BookListLogic", "class_book_shop_1_1_logic_1_1_book_list_logic.html", null ]
    ] ],
    [ "IClientChannel", null, [
      [ "BookShop.Program.MapsServiceReference.BookShopWebServiceChannel", "interface_book_shop_1_1_program_1_1_maps_service_reference_1_1_book_shop_web_service_channel.html", null ]
    ] ],
    [ "BookShop.Repository.IRepository< T >", "interface_book_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BookShop.Repository.IRepository< BasicInfos >", "interface_book_shop_1_1_repository_1_1_i_repository.html", [
      [ "BookShop.Repository.Repositories.BasicInfosRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_basic_infos_repository.html", null ]
    ] ],
    [ "BookShop.Repository.IRepository< BookShop.Data.BasicInfos >", "interface_book_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BookShop.Repository.IRepository< BookShop.Data.Categories >", "interface_book_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BookShop.Repository.IRepository< BookShop.Data.Shops >", "interface_book_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "BookShop.Repository.IRepository< Categories >", "interface_book_shop_1_1_repository_1_1_i_repository.html", [
      [ "BookShop.Repository.Repositories.CategoriesRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_categories_repository.html", null ]
    ] ],
    [ "BookShop.Repository.IRepository< Extras >", "interface_book_shop_1_1_repository_1_1_i_repository.html", [
      [ "BookShop.Repository.Repositories.ExtrasRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_extras_repository.html", null ]
    ] ],
    [ "BookShop.Repository.IRepository< Shops >", "interface_book_shop_1_1_repository_1_1_i_repository.html", [
      [ "BookShop.Repository.Repositories.ShopRepository", "class_book_shop_1_1_repository_1_1_repositories_1_1_shop_repository.html", null ]
    ] ],
    [ "BookShop.Logic.Interfaces.IShopListLogic", "interface_book_shop_1_1_logic_1_1_interfaces_1_1_i_shop_list_logic.html", [
      [ "BookShop.Logic.ShopListLogic", "class_book_shop_1_1_logic_1_1_shop_list_logic.html", null ]
    ] ],
    [ "BookShop.Program.MapsServiceReference.locationRequest", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request.html", null ],
    [ "BookShop.Program.MapsServiceReference.locationRequestBody", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_request_body.html", null ],
    [ "BookShop.Program.MapsServiceReference.locationResponse", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response.html", null ],
    [ "BookShop.Program.MapsServiceReference.locationResponseBody", "class_book_shop_1_1_program_1_1_maps_service_reference_1_1location_response_body.html", null ],
    [ "BookShop.Data.Shops", "class_book_shop_1_1_data_1_1_shops.html", null ],
    [ "BookShop.Logic.Tests.TestBookAndCategorySelectionLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_and_category_selection_logic.html", null ],
    [ "BookShop.Logic.Tests.TestBookListLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_book_list_logic.html", null ],
    [ "BookShop.Logic.Tests.TestFilterSet", "class_book_shop_1_1_logic_1_1_tests_1_1_test_filter_set.html", null ],
    [ "BookShop.Logic.Tests.TestShopListLogic", "class_book_shop_1_1_logic_1_1_tests_1_1_test_shop_list_logic.html", null ]
];