var class_book_shop_1_1_data_1_1_book_shop_d_b_entities =
[
    [ "BookShopDBEntities", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#a849837b7abae272a337b10316e5bb696", null ],
    [ "OnModelCreating", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#a1e7f4a026dedf4410d585c7dcb563164", null ],
    [ "BasicInfos", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#a439a1f50ee57e4b7e4f7c914524669ac", null ],
    [ "Categories", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#aa36d755af788e6762ccb09530468c42f", null ],
    [ "Extras", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#a13ee5c62bddcd70f2715ed5a51b7b26c", null ],
    [ "ExtrasInfos", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#ab7afdd1886696d9ae04baed11fef7498", null ],
    [ "Shops", "class_book_shop_1_1_data_1_1_book_shop_d_b_entities.html#af86fcb6c124ba9aa3ce2a9026c727f60", null ]
];